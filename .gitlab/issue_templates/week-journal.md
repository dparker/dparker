# Month :fallen_leaf: Wxx | MMM xx - MMM xx
[:arrow_backward:  Previous week](#2) • Next week :arrow_forward:

Current milestone: [xx](link)

## :palm_tree: Availability / Out of office (OoO)


* :ok_hand_tone3: Monday
* :ok_hand_tone3: Tuesday
* :ok_hand_tone3: Wednesday
* :ok_hand_tone3: Thursday
* :ok_hand_tone3: Friday

:thermometer: Out Sick
:sun_with_face: Family and Friends Day
:palm_tree: Vacation
:ferris_wheel: Public Holiday 
:ok_hand_tone3: Normal schedule: 7:00-15:00 UTC

_Read here about [my "normal" schedule and my working style](https://gitlab.com/michelletorres/readme#my-working-style)_
  


## :dart:  Goals

Important: _The following is my main focus for the week. Normal work like 1:1s, team meetings, following on comments, reviews, etc should continue happen._   

<details>
<summary>
    Mind process behind this prioritization
</summary>

Facts:  
1. 

Analyzing the facts:  
1. 

My reasoning to pick these goals:  
1. 

</details>
 


**What**  

* :one:  xyz  
* :two:  xyz

**How**  

1. [ ] xyz
    1. [ ] xyz
    1. [ ] xyz
    1. [ ] xyz
1. [ ] xyz
    1. [ ] xyz
    1. [ ] xyz
    1. [ ] xyz
1. [ ] xyz
    1. [ ] xyz
    1. [ ] xyz
    1. [ ] xyz


## :crystal_ball: Sometime in the future

<details>
<summary>
    Expand
</summary>

---
**To-Do:**



**To-Learn:**


</details>


## :star2: Wins of the week

- .
- .


## :nerd: Learnings and wow moments

1. .

